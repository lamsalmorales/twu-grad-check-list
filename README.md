The TWU Grad Check List currently launches with the Task page present. This app features an add button where the user can add in their desired bucket list. To do so, they can press the add button that will take them to a new page, the new tasks page where they can type their task. The tasks currently do not save and still need more functionality to be a complete app. 

Final Report

The TWU Grad List mobile application runs on any device with iOS software.The device that is being used to test this application is the iPhone 11. The TWU Grad List app is conveniently designed in the simplest way for users to maneuver through the app. To enter a task the user must type in the task they desire, this will add the task to the list that they have made. As the user adds each task, the task is prioritized by time/ importance. There will be a feature available to add the latitude and longitude by tapping on the task. Additionally, they can also edit the task name and priority when clicking on it. Once the user has completed the task, they can check it off by clicking the check box.
The process of creating the application through iOS came with many challenges. It was easy to maneuver through this particular software with the provided through canvas. Using Youtube videos was the key in helping us work through building the project. One area that we struggled on was being able to get the Add feature to work. Due to this, we had to combine codes amongst different resources found online and had to make sure it still ran through and met the requirements for the project. The challenges that we were presented with helped us learn how to work around it and the steps to resolve each issue. In doing so it has also helped us learn how to become better application developers.
*  REFERENCES
    *  Title: <todoListSwift>
    *  Author: <rileydnorris>
Availability: <https://github.com/rileydnorris/todoListSwift>
*
*  
